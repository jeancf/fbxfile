use env_logger;
use fbxfile::*;
use std::{fs::File, path::Path};

fn main() {
    let env = env_logger::Env::default()
        .filter_or("MY_LOG_LEVEL", "debug")
        .write_style_or("MY_LOG_STYLE", "always");

    env_logger::init_from_env(env);

    let mut file = File::open("testcases/buildings.fbx").unwrap();
    let contents = decode_fbx(&mut file).unwrap();

    print_nodes_tree(4, &contents, false);

    println!("\n**** Meshes in the file ****");
    let meshes: Vec<String> = get_nodes_by_id(&contents, "Model")
        .iter()
        .cloned()
        .map(|m| {
            if let Property::String(ref s) = m.properties[1] {
                s.trim_end_matches("\u{0}\u{1}Model").to_string()
            } else {
                panic!("Model node without name: {:?}", m)
            }
        })
        .collect();
    println!("{:?}", meshes);

    println!("\n**** Loading model in FbxFile struct ****");    
    let mut model = FbxModel::new(fbxfile::NormalMode::Vertex);
    model.load(Path::new("testcases/buildings.fbx"), Some("Ground"));
}
