use fbxfile::*;
use std::{fs::File, path::Path};

// Illustrates how to use the crate to access content of FBX file

fn main() {
    // Initialize logger
    env_logger::Builder::new()
        .filter_level(log::LevelFilter::Debug) // Default level for all modules
        .target(env_logger::Target::Stderr)
        .init();

    let mut file = File::open("testcases/cube_cyl.fbx").unwrap();
    let contents = decode_fbx(&mut file).unwrap();

    println!("\n**** flat dump of all content in file ****");
    println!("{:?}", contents);

    println!("\n**** Tree of node names ****");
    print_nodes_tree(0, &contents, true);

    println!("\n**** Tree of node \"Material\" displayed with properties ****");
    let material = get_nodes_by_id(&contents, "Material");
    print_nodes_tree(0, &material, false);

    println!("\n**** Meshes in the file ****");
    let meshes: Vec<String> = get_nodes_by_id(&contents, "Model")
        .iter()
        .cloned()
        .map(|m| {
            if let Property::String(ref s) = m.properties[1] {
                s.trim_end_matches("\u{0}\u{1}Model").to_string()
            } else {
                panic!("Model node without name: {:?}", m)
            }
        })
        .collect();
    println!("{:?}", meshes);

    // File contains 2 meshes. Isolate geometry node based on the name
    // of the mesh given in blender ("TESTCUBE")
    println!("\n**** Tree of selected \"TESTCUBE\" geometry node ****");
    let testcube_nodes = get_associated_nodes_by_model_name(&contents, "TESTCUBE").unwrap();
    print_nodes_tree(0, &[testcube_nodes[1].clone()], false);

    println!("\n**** Extracted vertex data from mesh ****");

    let vertices = get_nodes_by_id(&[testcube_nodes[1].clone()], "Vertices");
    //let vertices = get_properties_by_id(&cube_node, "Vertices");
    let varray = extract_F64Array(&vertices[0].properties[0]).expect("something went wrong");
    println!("Vertices ({}) : {:?}", varray.len(), varray);

    let indices =
        &get_nodes_by_id(&[testcube_nodes[1].clone()], "PolygonVertexIndex")[0].properties;
    let iarray = extract_I32Array(&indices[0]).expect("something went wrong");
    println!("Indices ({}) : {:?}", iarray.len(), iarray);

    let normals = &get_nodes_by_id(&[testcube_nodes[1].clone()], "Normals")[0].properties;
    let narray = extract_F64Array(&normals[0]).expect("something went wrong");
    println!("Normals ({}) : {:?}", narray.len(), narray);

    let uv = &get_nodes_by_id(&[testcube_nodes[1].clone()], "UV")[0].properties;
    let uarray = extract_F64Array(&uv[0]).expect("something went wrong");
    println!("UV ({}) : {:?}", uarray.len(), uarray);

    let uv_indices = &get_nodes_by_id(&[testcube_nodes[1].clone()], "UVIndex")[0].properties;
    let uiarray = extract_I32Array(&uv_indices[0]).expect("something went wrong");
    println!("UV Indices ({}) : {:?}", uiarray.len(), uiarray);

    println!();
    let mut cube = FbxModel::new(NormalMode::Split);
    cube.load(Path::new("testcases/cube_cyl.fbx"), Some("TESTCUBE"));
}
