use std::{fs::File, path::Path};

use nalgebra_glm as glm;

use super::*;

#[derive(Debug)]
pub enum NormalMode {
    Vertex,
    Split,
}

impl Default for NormalMode {
    fn default() -> Self {
        NormalMode::Split
    }
}

#[derive(Debug, Copy, Clone, PartialEq)]
#[repr(C)]
pub struct Vertex {
    /// spatial coordinates
    pub xyz: [f32; 3],
    /// normal
    pub ijk: [f32; 3],
    /// texture coordinates
    pub uv: [f32; 2],
}

#[derive(Default, Debug)]
pub struct FbxModel {
    pub normal_mode: NormalMode,
    pub vertices: Vec<Vertex>,
    pub indices: Vec<u32>,
    pub translation: [f32; 3],
    pub scaling: [f32; 3],
    pub rotation: [f32; 3],
    pub texture_file: Option<String>,
}

impl FbxModel {
    pub fn new(mode: NormalMode) -> Self {
        log::debug!("FbxModel initialized");
        Self {
            normal_mode: mode,
            vertices: vec![],
            indices: vec![],
            translation: [0.0; 3],
            scaling: [0.0; 3],
            rotation: [0.0; 3],
            texture_file: None,
        }
    }

    /// Open file for reading and load data for Model with `model` name
    /// If `model` is omitted data for the first model found in the file will be loaded
    /// ATTENTION: Faces of the model cannot be convex
    pub fn load(&mut self, filename: &Path, model: Option<&str>) {
        let mut file = File::open(filename).expect("Could not open file");
        let contents = decode_fbx(&mut file).unwrap();

        let model_node = match model {
            Some(s) => vec![get_model_node_by_name(&contents, &s).unwrap()],
            None => {
                let objects_node = get_nodes_by_id(&contents, "Objects");
                let model_nodes = get_nodes_by_id(&objects_node, "Model");
                vec![model_nodes[0].clone()]
            }
        };

        if let Property::String(s) = &model_node[0].properties[2] {
            if s != "Mesh" {
                panic!("Model is of type \"{}\" and not \"Mesh\"", s);
            }
        };

        let model_name = if let Property::String(s) = &model_node[0].properties[1] {
            s.trim_end_matches("\u{0}\u{1}Model")
        } else {
            panic!("Invalid Model node: {:?}", model_node);
        };

        log::info!("Loading model {} in {:?}", model_name, filename.file_name().unwrap());
        log::debug!("Model node: {:?}", model_node);

        // Get list of P nodes under Model node
        let p_nodes = get_nodes_by_id(&model_node, "P");
        log::debug!("P nodes in Model node {:?}", p_nodes);

        // iterate P nodes and extract transforms
        for p in p_nodes {
            let transform = if let Property::String(t) = &p.properties[0] {
                t
            } else {
                panic!("Invalid P node {:?}", p);
            };

            match transform.as_str() {
                "Lcl Translation" => {
                    let tx = if let Property::F64(x) = p.properties[4] {
                        x as f32
                    } else {
                        panic!("Invalid P node {:?}", p)
                    };
                    let ty = if let Property::F64(y) = p.properties[5] {
                        y as f32
                    } else {
                        panic!("Invalid P node {:?}", p)
                    };
                    let tz = if let Property::F64(z) = p.properties[6] {
                        z as f32
                    } else {
                        panic!("Invalid P node {:?}", p)
                    };
                    self.translation = [tx, ty, tz];
                    log::debug!("Extracted translation: {:?}", self.translation);
                }
                "Lcl Rotation" => {
                    let rx = if let Property::F64(x) = p.properties[4] {
                        x as f32
                    } else {
                        panic!("Invalid P node {:?}", p)
                    };
                    let ry = if let Property::F64(y) = p.properties[5] {
                        y as f32
                    } else {
                        panic!("Invalid P node {:?}", p)
                    };
                    let rz = if let Property::F64(z) = p.properties[6] {
                        z as f32
                    } else {
                        panic!("Invalid P node {:?}", p)
                    };
                    self.rotation = [rx, ry, rz];
                    log::debug!("Extracted rotation: {:?}", self.rotation);
                }
                "Lcl Scaling" => {
                    let sx = if let Property::F64(x) = p.properties[4] {
                        x as f32
                    } else {
                        panic!("Invalid P node {:?}", p)
                    };
                    let sy = if let Property::F64(y) = p.properties[5] {
                        y as f32
                    } else {
                        panic!("Invalid P node {:?}", p)
                    };
                    let sz = if let Property::F64(z) = p.properties[6] {
                        z as f32
                    } else {
                        panic!("Invalid P node {:?}", p)
                    };
                    self.scaling = [sx, sy, sz];
                    log::debug!("Extracted scaling: {:?}", self.scaling);
                }
                _ => {}
            }
        }

        // Get associated nodes
        let assoc_nodes = get_associated_nodes_by_model_name(&contents, model_name).unwrap();

        // Get Texture file name from first Texture node in the list of associated nodes
        let tn = assoc_nodes
            .iter()
            .find(|n| n.id == "Texture");

        self.texture_file = match tn {
            Some(node) => {
                // Get FileName node from texture node chain
                let rel_fn = get_nodes_by_id(&vec![node.clone()], "FileName");
                if let Property::String(s) = &rel_fn[0].properties[0] {
                    Some(s.to_string())
                 }
                 else {
                     None
                 }
            },
            None => None,
        };

         log::debug!("Texture filename: {:?}",self.texture_file);

        // Get the first node with ID "Geometry" in the list of associated nodes
        let geometry_node = vec![assoc_nodes
            .iter()
            .find(|n| n.id == "Geometry")
            .unwrap()
            .clone()];

        let vert = &get_nodes_by_id(&geometry_node, "Vertices")[0].properties;
        let vert_lst = extract_F64Array(&vert[0]).expect("something went wrong");
        log::debug!("Vertices ({}) : {:?}", vert_lst.len(), vert_lst);

        let vert_idx = &get_nodes_by_id(&geometry_node, "PolygonVertexIndex")[0].properties;
        let vert_idx_lst = extract_I32Array(&vert_idx[0]).expect("something went wrong");
        log::debug!("Indices ({}) : {:?}", vert_idx_lst.len(), vert_idx_lst);

        let textcoord = &get_nodes_by_id(&geometry_node, "UV");
        let textcoord_lst = if !textcoord.is_empty() {
            extract_F64Array(&textcoord[0].properties[0]).expect("something went wrong")
        } else {
            // Default value if no UV in file
            vec![0.0; 3]
        };
        log::debug!("UV ({}) : {:?}", textcoord_lst.len(), textcoord_lst);

        let textcoord_idx = &get_nodes_by_id(&geometry_node, "UVIndex");
        let textcoord_idx_lst = // Default value if no UV in file
        if !textcoord.is_empty() {
            extract_I32Array(&textcoord_idx[0].properties[0]).expect("something went wrong")
        } else {
            vec![0 as i32; vert_idx_lst.len()]
        };
        log::debug!(
            "UV Indices ({}) : {:?}",
            textcoord_idx_lst.len(),
            textcoord_idx_lst
        );

        let norm = get_nodes_by_id(&geometry_node, "Normals");
        let norm_lst = extract_F64Array(&norm[0].properties[0]).expect("something went wrong");
        log::debug!("Split normals ({}) : {:?}", norm_lst.len(), norm_lst);

        // Overwrite lists with new ones including only triangles
        log::debug!("Triangulating faces...");
        let (vert_idx_lst, textcoord_idx_lst, norm_lst) =
            self.convert_to_tris(&vert_lst, &vert_idx_lst, &textcoord_idx_lst, &norm_lst);

        log::debug!("** After triangulation **");
        log::debug!("vert_lst ({}): {:?}", vert_lst.len(), vert_lst);
        log::debug!("vert_idx_lst ({}): {:?}", vert_idx_lst.len(), vert_idx_lst);
        log::debug!(
            "textcoord_idx_lst({}): {:?}",
            textcoord_idx_lst.len(),
            textcoord_idx_lst
        );
        log::debug!("norm_lst ({}): {:?}", norm_lst.len(), norm_lst);

        // Check that all faces are triangles and panic if not
        // TODO  Remove once convert_to_tris() is reliable
        //Self::check_only_triangles(&vert_idx_lst);

        let vert_norm_lst = self.get_vtx_normals_lst(&vert_lst, &vert_idx_lst, &norm_lst);

        // Assemble final vertex list by iterating
        // vertex index list an UV index list in parallel
        for (i, vi) in vert_idx_lst.iter().enumerate() {
            // Get spatial and normal data
            let mut vertex = vert_norm_lst[if *vi < 0 { *vi * -1 - 1 } else { *vi } as usize];
            // Add corresponding UV coordinates to vertex data
            vertex.uv = [
                textcoord_lst[(textcoord_idx_lst[i as usize] * 2) as usize] as f32,
                textcoord_lst[(textcoord_idx_lst[i as usize] * 2 + 1) as usize] as f32,
            ];
            // Add corresponding normal coordinates to vertex data
            if let NormalMode::Split = self.normal_mode {
                vertex.ijk = [
                    norm_lst[i as usize * 3] as f32,
                    norm_lst[i as usize * 3 + 1] as f32,
                    norm_lst[i as usize * 3 + 2] as f32,
                ];
            }
            // Insert vertex data in list if not there yet
            let mut found: Option<usize> = None;
            for (i, v) in self.vertices.iter().enumerate() {
                if *v == vertex {
                    found = Some(i);
                }
            }

            match found {
                Some(idx) => {
                    self.indices.push(idx as u32);
                }
                None => {
                    self.indices.push(self.vertices.len() as u32);
                    self.vertices.push(vertex);
                }
            }
        }

        log::info!("Model loaded ({})", self.vertices.len());
        for v in &self.vertices {
            log::debug!("{:?}", v);
        }
        log::debug!("Indices ({}): {:?}", self.indices.len(), self.indices);
    }

    /// Iterate each vertex to calculate and associate vertex normal
    fn get_vtx_normals_lst(
        &self,
        vert_lst: &Vec<f64>,
        vert_idx_lst: &Vec<i32>,
        norm_lst: &Vec<f64>,
    ) -> Vec<Vertex> {
        let mut vert_norm_lst = vec![];
        for vl_pos in 0..vert_lst.len() / 3 {
            let mut vert_norm = [0.0, 0.0, 0.0];

            // populate vert_norm if we want vertex normal
            if let NormalMode::Vertex = self.normal_mode {
                let mut vert_nl_list = vec![];
                // Collect normals for each vertex and remove duplicates
                for (i, vil_pos) in vert_idx_lst.iter().enumerate() {
                    let pos_vil_pos = if *vil_pos < 0 {
                        *vil_pos * -1 - 1
                    } else {
                        *vil_pos
                    };
                    if pos_vil_pos as usize == vl_pos {
                        // Candidate normal
                        let cand_nl = [
                            norm_lst[(i * 3) as usize],
                            norm_lst[(i * 3 + 1) as usize],
                            norm_lst[(i * 3 + 2) as usize],
                        ];
                        // Add unique normal to list
                        if !vert_nl_list.contains(&cand_nl) {
                            vert_nl_list.push(cand_nl);
                        }
                    }
                }
                // Add normals up and normalize them
                vert_norm = {
                    let mut i = 0.0;
                    let mut j = 0.0;
                    let mut k = 0.0;
                    for v in vert_nl_list {
                        i += v[0];
                        j += v[1];
                        k += v[2];
                    }

                    let magnitude = (i.powi(2) + j.powi(2) + k.powi(2)).sqrt();
                    i /= magnitude;
                    j /= magnitude;
                    k /= magnitude;
                    [i as f32, j as f32, k as f32]
                };
            }

            vert_norm_lst.push(Vertex {
                xyz: [
                    vert_lst[vl_pos * 3 as usize] as f32,
                    vert_lst[vl_pos * 3 + 1 as usize] as f32,
                    vert_lst[vl_pos * 3 + 2 as usize] as f32,
                ],
                ijk: vert_norm,
                uv: [0.0, 0.0],
            })
        }
        log::debug!("Converted face normals to vertex normals");
        log::debug!(
            "Vertex normals ({}) : {:?}",
            vert_norm_lst.len(),
            vert_norm_lst
        );
        vert_norm_lst
    }

    /// Iterate the list of vertex indices from the fbx model
    /// and count that there are max. 3 indices between
    /// negative values (that indicate the end of a polygon)
    fn _check_only_triangles(vertex_indices: &[i32]) {
        let mut count = 0;
        for vi in vertex_indices {
            if *vi >= 0 {
                count += 1;
            } else {
                count = 0;
            }

            assert!(
                count < 3,
                "Model contains at least one polygon with more \
                 than 3 vertices, which is not yet supported"
            );
        }
        log::debug!("Faces verified to contain only triangles");
    }

    /// Iterate vertex indices and convert faces with more than 3 vertices
    /// to triangles. Modifies indices and normals accordingly
    #[allow(clippy::ptr_arg)]
    pub fn convert_to_tris(
        &mut self,
        vert_lst: &Vec<f64>,
        vert_idx_lst: &Vec<i32>,
        textcoord_idx_lst: &Vec<i32>,
        norm_lst: &Vec<f64>,
    ) -> (Vec<i32>, Vec<i32>, Vec<f64>) {
        // Vectors to hold data of triangles to be returned
        let mut n_vert_idx_lst: Vec<i32> = vec![];
        let mut n_textcoord_idx_lst: Vec<i32> = vec![];
        let mut n_norm_lst: Vec<f64> = vec![];

        // Temporary vectors to store data related to each face
        let mut vil = vec![];
        let mut til = vec![];
        let mut nl = vec![];

        for (i, vi) in vert_idx_lst.iter().enumerate() {
            // Add vertex data to temporary vectors
            vil.push(vert_idx_lst[i]);
            til.push(textcoord_idx_lst[i]);
            nl.push(norm_lst[i * 3]);
            nl.push(norm_lst[i * 3 + 1]);
            nl.push(norm_lst[i * 3 + 2]);

            // Vertex index is negative, signaling end of face
            if *vi < 0 {
                // Not a triangle
                if vil.len() > 3 {
                    log::trace!("Triangulating {:?}...", &vil);
                    let (mut n_vil, mut n_til, mut n_nl) =
                        self.triangulate(&vert_lst, &mut vil, &mut til, &mut nl);
                    log::trace!("Done {:?}", n_vil);
                    // Append data of resulting triangles to new lists
                    n_vert_idx_lst.append(&mut n_vil);
                    n_textcoord_idx_lst.append(&mut n_til);
                    n_norm_lst.append(&mut n_nl);
                    // Empty temporary vectors
                    vil.clear();
                    til.clear();
                    nl.clear();
                } else {
                    // we have a triangle
                    // Append data as-is to new lists
                    n_vert_idx_lst.append(&mut vil);
                    n_textcoord_idx_lst.append(&mut til);
                    n_norm_lst.append(&mut nl);
                }
            }
        }
        (n_vert_idx_lst, n_textcoord_idx_lst, n_norm_lst)
    }

    /// Process a single face and convert it to triangles
    /// Return lists of indices triangles
    #[allow(clippy::ptr_arg)]
    fn triangulate(
        &mut self,
        vert_lst: &Vec<f64>,
        vil: &mut Vec<i32>,
        til: &mut Vec<i32>,
        nl: &mut Vec<f64>,
    ) -> (Vec<i32>, Vec<i32>, Vec<f64>) {
        // Create destination vectors
        // that will receive triangle data
        let mut n_vil: Vec<i32> = vec![];
        let mut n_til: Vec<i32> = vec![];
        let mut n_nl: Vec<f64> = vec![];

        // Load vertices of face in glm structs
        let mut vert3d_lst: Vec<glm::DVec3> = vec![];

        for vi in vil.iter_mut() {
            // *vi is negative - 1 if it is the last vertex
            let pvi = if *vi < 0 {
                ((*vi * -1) - 1) as usize
            } else {
                *vi as usize
            };

            // Make sure we have only positive vertex indices in vil
            *vi = pvi as i32;

            vert3d_lst.push(glm::vec3(
                vert_lst[(pvi) * 3],
                vert_lst[(pvi) * 3 + 1],
                vert_lst[(pvi) * 3 + 2],
            ));
        }

        // Convert 3D vertex coordinates to local 2D coordinates
        let mut vert2d_lst = Self::get_local_coordinates(&vert3d_lst);

        let mut v2dl_len = vert2d_lst.len();
        // Define winding of face
        let face_is_cw: bool = {
            // find index of top-leftmost vertex of face
            let mut topleftmost: usize = 0;
            for i in 1..v2dl_len {
                // Select the one with the smallest x
                if vert2d_lst[i].x < vert2d_lst[topleftmost].x {
                    topleftmost = i;
                }
                // If draw on x, select the one with smallest y
                // Allow small margin of error in comparison of float
                else if (vert2d_lst[i].x - vert2d_lst[topleftmost].x).abs() < std::f64::EPSILON
                    && vert2d_lst[i].y < vert2d_lst[topleftmost].y
                {
                    topleftmost = i
                }
            }

            // Cross product of 2 vectors from top-leftmost vertex
            // We are sure that they are not colinear thanks to selection of top-leftmost
            let vector1 = vert2d_lst[(topleftmost + 1) % v2dl_len] - vert2d_lst[topleftmost];
            let vector2 = vert2d_lst[topleftmost]
                - vert2d_lst[if topleftmost == 0 {
                    v2dl_len - 1
                } else {
                    topleftmost - 1
                }];

            // Cross product of 2 consecutive vectors gives magnitude of perpendicular
            // Sign of magnitude represents winding: positive -> CW, negative -> CCW, 0 -> colinear
            vector1.x * vector2.y - vector1.y * vector2.x > 0.0
        };

        // Process list until there are less than 3 vertices left
        while v2dl_len > 3 {
            // Iterate vertices of face looking for an ear to cut
            // TODO let start = 0
            for i in 0..v2dl_len {
                // Calculate winding of triangle formed by vertices i, i+1 (ear) and i+2
                let tri_winding = {
                    let vector1 = vert2d_lst[(i + 2) % v2dl_len] - vert2d_lst[(i + 1) % v2dl_len];
                    let vector2 = vert2d_lst[(i + 1) % v2dl_len] - vert2d_lst[i];
                    vector1.x * vector2.y - vector1.y * vector2.x
                };
                // Is it the same winding as the face?
                if (tri_winding > 0.0) == face_is_cw {
                    // found ear in position i+1
                    let vil_l = vil.len();
                    let til_l = til.len();
                    // Insert vertices of ear triangle in new lists
                    n_vil.push(vil[i]);
                    n_vil.push(vil[(i + 1) % vil_l]);
                    n_vil.push(-(vil[(i + 2) % vil_l] + 1));
                    // Last vertex must be negative of (index + 1)

                    n_til.push(til[i]);
                    n_til.push(til[(i + 1) % til_l]);
                    n_til.push(til[(i + 2) % til_l]);

                    n_nl.push(nl[3 * i]);
                    n_nl.push(nl[(3 * i) + 1]);
                    n_nl.push(nl[(3 * i) + 2]);

                    n_nl.push(nl[3 * ((i + 1) % vil_l)]);
                    n_nl.push(nl[(3 * ((i + 1) % vil_l)) + 1]);
                    n_nl.push(nl[(3 * ((i + 1) % vil_l)) + 2]);

                    n_nl.push(nl[3 * ((i + 2) % vil_l)]);
                    n_nl.push(nl[(3 * ((i + 2) % vil_l)) + 1]);
                    n_nl.push(nl[(3 * ((i + 2) % vil_l)) + 2]);

                    // Remove ear from face
                    vert2d_lst.remove((i + 1) % v2dl_len);
                    vil.remove((i + 1) % vil_l);
                    til.remove((i + 1) % til_l);
                    nl.remove(3 * ((i + 1) % vil_l));
                    nl.remove(3 * ((i + 1) % vil_l));
                    nl.remove(3 * ((i + 1) % vil_l));
                    break;
                }
            }
            v2dl_len = vert2d_lst.len();
            // TODO start = (start + 1) % v2d_len  // move start vertex to avoid triangle fan
        }
        // Add last 3 vertices to new lists
        for i in 0..3 {
            // Insert vertices of ear triangle to new lists
            if i == 2 {
                // Last vertex must be negative of index
                n_vil.push(-(vil[i] + 1));
            } else {
                n_vil.push(vil[i]);
            }

            n_til.push(til[i]);

            n_nl.push(nl[3 * i]);
            n_nl.push(nl[(3 * i) + 1]);
            n_nl.push(nl[(3 * i) + 2]);
        }
        (n_vil, n_til, n_nl)
    }

    /// Get a list of 3d vertices and convert it to a list of 2d vertices
    /// that correspond to the local coordinates in the space of the polygon
    fn get_local_coordinates(vert3d_lst: &[glm::DVec3]) -> Vec<glm::DVec3> {
        let mut vert2d_lst: Vec<glm::DVec3> = vec![];

        // Find 3 non-colinear vertices an use them to define local axes
        let v_l_len = vert3d_lst.len();
        'outer: for i in 0..v_l_len {
            let loc_org: glm::DVec3 = vert3d_lst[(i + 1) % v_l_len];
            // Note: we are using the modulo as we have a circular list

            // Calculate normal to the 2 vectors defined by consecutive vertices
            let mut loc_x: glm::DVec3 = vert3d_lst[i % v_l_len] - loc_org;
            let v2: glm::DVec3 = vert3d_lst[(i + 2) % v_l_len] - loc_org;
            let loc_n: glm::DVec3 = loc_x.cross(&v2);

            // Verify that the magnitude of the normal is not zero (0 -> colinear vectors). If it is, loop
            if glm::length(&loc_n) != 0.0 {
                // Calculate local Y axis
                let mut loc_y: glm::DVec3 = loc_n.cross(&loc_x);

                // Normalize axes
                loc_x /= glm::length(&loc_x);
                loc_y /= glm::length(&loc_y);

                for vert3d in vert3d_lst {
                    // Calculate 2d coordinates of vertex
                    let vert2d = glm::DVec3::new(
                        (vert3d - loc_org).dot(&loc_x),
                        (vert3d - loc_org).dot(&loc_y),
                        0.0,
                    );

                    // Add 2d vertex to list
                    vert2d_lst.push(vert2d);
                }
                // Verify that the polygon is not self-intersecting
                if !is_self_intersecting(&vert2d_lst) {
                    break 'outer;
                } else {
                    vert2d_lst.clear();
                }
            }
        }
        vert2d_lst
    }
}

/// Check if the 2d polygon is self intersecting or not
// https://algs4.cs.princeton.edu/91primitives/
fn is_self_intersecting(face: &[glm::DVec3]) -> bool {
    // Iterate vertices of the face and use as base of first edge
    for base1_idx in 0..face.len() - 1 {
        let vector1 = face[base1_idx + 1] - face[base1_idx];
        // Iterate other edges
        let range = if base1_idx == 0 {
            base1_idx + 2..face.len() - 1
        } else {
            base1_idx + 2..face.len()
        };

        for base2_idx in range {
            // Test if second edge is on one side of the first
            let v1 = face[base2_idx] - face[base1_idx + 1];
            let v2 = face[(base2_idx + 1) % face.len()] - face[base1_idx + 1];
            let t1 = vector1.cross(&v1);
            let t2 = vector1.cross(&v2);
            let test1 = t1.dot(&t2);

            // if it is not
            if test1 <= 0.0 {
                // test if first edge is on one side of first one
                let vector2 = face[(base2_idx + 1) % face.len()] - face[base2_idx];
                let v1 = face[base1_idx] - face[(base2_idx + 1) % face.len()];
                let v2 = face[base1_idx + 1] - face[(base2_idx + 1) % face.len()];
                let t1 = vector2.cross(&v1);
                let t2 = vector2.cross(&v2);
                let test2 = t1.dot(&t2);
                // if it is not
                if test2 <= 0.0 {
                    // We have found intersecting vertices
                    return true;
                }
            }
        }
    }
    false
}

#[test]
fn intersecting_faces() {
    let face = vec![
        glm::DVec3::new(0.0, 0.0, 0.0),
        glm::DVec3::new(0.0, 3.0, 0.0),
        glm::DVec3::new(1.0, -1.0, 0.0),
        glm::DVec3::new(2.0, 0.0, 0.0),
    ];

    assert_eq!(is_self_intersecting(&face), true);
}
