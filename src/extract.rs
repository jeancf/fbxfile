use super::*;

/// Print tree of node id's found in `contents`
/// with `num_spaces` spaces indentation for each new level
pub fn print_nodes_tree(mut num_spaces: i8, contents: &[Node], only_ids: bool) {
    for node in contents.iter() {
        for _ in 0..num_spaces {
            print!("·");
        }

        if only_ids {
            print!("{} ({})", &node.id, node.subnodes.len());
            if node.id == "P" {
                if let Property::String(ref s) = node.properties[0] {
                    print!(": {}", s);
                    num_spaces -= 1;
                }
            }
        } else {
            print!("{}: {:?}", &node.id, &node.properties);
            if node.id == "P" {
                num_spaces -= 1;
            }
        }

        println!();
        num_spaces += 1;

        print_nodes_tree(num_spaces, &node.subnodes, only_ids);
        num_spaces -= 1;
    }
}

/// Return vector with nodes with the id provided (exact match)
/// Recurse the whole node tree to find them
pub fn get_nodes_by_id(contents: &[Node], id: &str) -> Vec<Node> {
    let mut results = vec![];
    get_nbi(&contents, &id, &mut results);
    results
}

/// Called by get_nodes_by_id()
fn get_nbi(contents: &[Node], id: &str, results: &mut Vec<Node>) {
    for node in contents.iter() {
        if node.id == id {
            results.push(node.clone());
        }
        get_nbi(&node.subnodes, id, results);
    }
}

/// Search string in nodes "Model" for name given in blender then load data from corresponding Model node
pub fn get_model_node_by_name(contents: &[Node], name: &str) -> Result<Node, &'static str> {
    // Collect short list of Model nodes
    let model_nodes = get_nodes_by_id(&contents, "Model");

    model_nodes
        .into_iter()
        .filter(|n| {
            if let Property::String(s) = &n.properties[1] {
                // Isolate model name
                let model_name = s.trim_end_matches("\u{0}\u{1}Model");
                // Compare to what we are looking for
                if model_name == name {
                    return true;
                } else {
                    return false;
                }
            } else {
                return false;
            }
        })
        .nth(0)
        .ok_or("Model node of that name not found in file")
}

/// Search string in nodes "Geometry" for name given in blender then load data from corresponding Geometry node
pub fn get_geometry_node_by_name(contents: &[Node], name: &str) -> Result<Node, &'static str> {
    // Collect short list of Geometry nodes
    let geo_nodes = get_nodes_by_id(&contents, "Geometry");

    geo_nodes
        .into_iter()
        .filter(|n| {
            if let Property::String(s) = &n.properties[1] {
                // Isolate model name
                let model_name = s.trim_end_matches("\u{0}\u{1}Geometry");
                // Compare to what we are looking for
                if model_name == name {
                    return true;
                } else {
                    return false;
                }
            } else {
                return false;
            }
        })
        .nth(0)
        .ok_or("Geometry node of that name not found in file")
}

// Search string in nodes "Model" for name given in blender and return associated Geometry node
pub fn get_associated_nodes_by_model_name(
    contents: &[Node],
    name: &str,
) -> Result<Vec<Node>, &'static str> {
    // Collect short list of model nodes
    let model_nodes = get_nodes_by_id(&contents, "Model");

    let mut node_vec = Vec::<Node>::new();

    // Add matching Model node to return vector
    for node in model_nodes {
        if let Property::String(s) = &node.properties[1] {
            let node_name = s.trim_end_matches("\u{0}\u{1}Model");
            if name == node_name {
                node_vec.push(node.clone());
            }
        }
    }

    // Collect serial numbers of nodes associated with model
    let model_serno = if let Property::I64(serno) = node_vec[0].properties[0] {
        serno
    } else {
        0
    };
    let assoc_sernos = get_associated_sernos(contents, model_serno);

    // Look for Geometry nodes with these serial numbers
    let geo_nodes = get_nodes_by_id(&contents, "Geometry");
    for asn in assoc_sernos.iter() {
        for gn in &geo_nodes {
            if let Property::I64(sn) = gn.properties[0] {
                if sn == *asn {
                    node_vec.push(gn.clone());
                    break;
                }
            }
        }
    }

    // Get other associated nodes
    // Model -> Geometry **DONE**
    //       -> Materials -> Textures
    
    // Gather Material nodes with these serial numbers
    let mut mat_sernos = vec![];
    let mat_nodes = get_nodes_by_id(&contents, "Material");
    for asn in assoc_sernos.iter() {
        for mn in &mat_nodes {
            if let Property::I64(sn) = mn.properties[0] {
                if sn == *asn {
                    mat_sernos.push(sn);
                    node_vec.push(mn.clone());
                    break;
                }
            }
        }
    }

    // Gather Texture nodes referenced by material nodes
    // FIXME: the link between material and texture is not captured
    let mat_assoc_sernos: Vec<i64> = mat_sernos
        .iter()
        .map( |ms| { 
            get_associated_sernos(contents,*ms)
        })
        .flatten()
        .collect();
    
    // Collect Texture nodes with these serial numbers
    let tex_nodes = get_nodes_by_id(&contents, "Texture");
    for masn in mat_assoc_sernos.iter() {
        for tn in &tex_nodes {
            if let Property::I64(sn) = tn.properties[0] {
                if sn == *masn {
                    node_vec.push(tn.clone());
                    break;
                }
            }
        }
    }

    log::trace!("Collected nodes: {:?}", node_vec);

    Ok(node_vec)
}

fn get_associated_sernos(nodes: &[Node], serno: i64) -> Vec<i64> {
    let connections = get_nodes_by_id(&nodes, "Connections");
    let cs = get_nodes_by_id(&connections, "C");

    cs.iter()
        .filter(|c| {
            if let Property::I64(s) = c.properties[2] {
                s == serno
            } else {
                false
            }
        })
        .map(|c| {
            if let Property::I64(s) = c.properties[1] {
                s
            } else {
                0
            }
        })
        .collect()
}

/// Return a vector of f64 extracted from a F64Array property
pub fn extract_F64Array(src: &Property) -> Result<Vec<f64>, &str> {
    if let Property::F64Array(ref rst) = src {
        Ok(rst.clone())
    } else {
        Err("Could not extract f64 array")
    }
}

/// Return a vector of i32 extracted from an I32Array property
pub fn extract_I32Array(src: &Property) -> Result<Vec<i32>, &str> {
    if let Property::I32Array(ref rst) = src {
        Ok(rst.clone())
    } else {
        Err("Could not extract i32 array")
    }
}
