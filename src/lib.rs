#![allow(non_snake_case)]
//! **fbxfile** is a lightweight crate that provides tools for the manipulation
//! of FBX files. It is based on the implementation of the FBX file format
//! supported by blender.
//!
//! It provides:
//! * a set of functions to parse the document model and extract data
//! * a convenience struct ([`FbxModel`](./fbxmodel/struct.FbxModel.html)) that simplifies the extraction of a
//! model for use in a 3D application including:
//!     * conversion of quads to triangles
//!     * conversion of face normals to vertex normals
//!
//! _This crate does not support writing to FBX files at the moment._

pub mod decode;
pub use decode::*;

pub mod extract;
pub use extract::*;

pub mod fbxmodel;
pub use fbxmodel::*;

#[cfg(test)]
mod tests;
